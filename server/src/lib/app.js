import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import logger from 'morgan';
import _ from 'lodash';
import * as http from 'http';
import {ApplicationController} from './core/controller';

/**
 * Logger format type.
 * @enum {string}
 */
export const AppConfigLogger = {
	combined: 'combined',
	common: 'common',
	dev: 'dev',
	short: 'short',
	tiny: 'tiny',
};

/**
 * Application config.
 * @typedef {Object} AppConfig
 * @property {number} [port] - port to run application on
 * @property {AppConfigLogger} [logger] - logger format
 * @property {Array<ApplicationController> | Object<string, ApplicationController>} controllers - list of controllers for application
 */

/**
 * Application class, wrap around express app using ES7 decorators
 * @class
 */
export class Application {

	/**
	 * Pointer to single instance of application
	 * @type {Application}
	 */
	static instance = null;

	/**
	 * Server application bootstrap
	 * @param {AppConfig} [config]
	 * @returns {Application}
	 */
	static bootstrap(config) {
		return Application.getInstance(config);
	}

	/**
	 * Method to get singleton application
	 * @param {AppConfig} [config]
	 * @returns {Application}
	 */
	static getInstance(config) {
		if(!Application.instance) {
			Application.instance = new Application(config);
		}

		return Application.instance;
	}

	/**
	 * @type {AppConfig}
	 */
	config = {
		port: 8081,
		logger: AppConfigLogger.dev
	};

	/**
	 * Express app instance
	 * @type {*|Function}
	 */
	app = express();

	/**
	 * @type {Server}
	 */
	server = http.createServer(this.app);

	/**
	 * Create application instance
	 * @param {AppConfig} [config]
	 */
	constructor(config = {}) {
		this.config = _.cloneDeep(config, this.config);

		this.applyMiddlewares();
		this.applyRouterRoutes();
		this.applyErrorHandlers();

		this.app.set('port', this.config.port);
		this.server.listen(this.config.port);
	}

	applyRouterRoutes() {
		const controllers = this.config.controllers;

		_.forEach(controllers, controllerClass => this.initController(controllerClass));
	}

	/**
	 * @param {ApplicationController} controllerClass
	 */
	initController(controllerClass) {
		const controller = new controllerClass();

		if(controller instanceof ApplicationController) {
			const controllerRouter = express.Router();
			const prefix = controller.constructor._prefix;

			if(controller.constructor._middleware.length) {
				controllerRouter.use(controller.constructor._middleware.map(middleware => middleware.bind(controller)));
			}

			controller.constructor._routes.forEach(route => {
				const method = route.method;
				const path = route.path;
				const action = controller[route.name];

				if(!action) return;

				if(route.middleware.length) {
					controllerRouter.use(route.middleware.map(middleware => middleware.bind(controller)));
				}

				controllerRouter[method](path, action.bind(controller));
			});

			this.app.use(prefix, controllerRouter);
		}
	}

	/**
	 * Apply global middlewares for application
	 */
	applyMiddlewares() {
		this.app.use(logger(this.config.logger));
		this.app.use(cors());
		this.app.use(bodyParser.json());
		this.app.use(bodyParser.text());
		this.app.use(bodyParser.urlencoded({ extended: false }));
	}

	/**
	 * Apply global error handlers
	 */
	applyErrorHandlers() {
		this.app.use((req, res) => {
			res.status(404);
			res.json();
		});

		this.app.use(function(err, req, res) {
			res.status(500);
			res.json();
		});
	}
}
