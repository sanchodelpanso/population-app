/**
 * Request decorator options.
 * @typedef {Object} RequestControllerOptions
 * @property {string} [prefix] - controller routes prefix
 * @property {Function|Array<Function>} [middleware] - controller routes middleware, to use class context inside middleware, declare with 'function' not () => {}
 */

/**
 * Request class decorator
 * @param {RequestControllerOptions} [options]
 * @returns {Function}
 * @constructor
 */
export function RequestController(options = {}) {
	/**
	 * @param {ApplicationController} target - decorated class
	 */
	return function(target) {
		target._prefix = options.prefix || '';
		target._middleware.concat(options.middleware || []);
	};
}