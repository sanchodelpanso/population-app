/**
 * Request decorator options.
 * @typedef {Object} RequestMappingOptions
 * @property {string} path - request path
 * @property {RequestMethod} [method=RequestMethod.GET] - request method
 * @property {Function|Array<Function>} [middleware] - request middleware, to use class context inside middleware, declare with 'function' not () => {}
 */

import {RequestMethod} from '../core/request-method';

/**
 * Request method decorator
 * @param {RequestMappingOptions} [options]
 * @returns {Function}
 * @constructor
 */
export function RequestMapping(options = {}) {
	/**
	 * @param {ApplicationController} target - decorated method class
	 * @param {string} key - method name
	 */
	return function(target, key) {
		if(!(target[key] instanceof Function)) {
			const msg = `${target.constructor.name}.${key} is not class method, @RequestMapping could be applied only for methods`;
			throw new Error(msg);
		}

		if(!options.path) {
			const msg = `${target.name}.${key}() requires 'path' property to be defined`;
			throw new Error(msg);
		}

		target.constructor._routes.push({
			method: options.method || RequestMethod.GET,
			path: options.path,
			middleware: [].concat(options.middleware || []),
			name: key
		});
	};
}