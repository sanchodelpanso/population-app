export class ApplicationController {
	/**
	 * Prefix for all controller routes
	 * @type {string}
	 * @public
	 */
	static _prefix = '';
	/**
	 * List of middleware functions, applied to each class method
	 * @type {Array<Function>}
	 * @public
	 */
	static _middleware = [];

	/**
	 * List of routes
	 * @type {Array<Object>}
	 * @public
	 */
	static _routes = [];

	notFound() {}

	badRequest() {}
}