/**
 * Request methods enum.
 * @enum {string}
 */
export const RequestMethod = {
	GET: 'get',
	POST: 'post',
	PUT: 'put',
	PATCH: 'patch',
	DELETE: 'delete',
};