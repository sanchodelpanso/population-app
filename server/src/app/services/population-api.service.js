import request from 'request-promise';
import moment from 'moment';
import * as _ from 'lodash';

/**
 * Describes population in particular year.
 * @typedef {Object} YearPopulation
 * @property {number} [year]
 * @property {number} [total]
 * @property {number} [male]
 * @property {number} [female]
 */

/**
 * Describes population in particular date.
 * @typedef {Object} DatePopulation
 * @property {number} [year]
 * @property {number} [population]
 * @property {string} [country]
 */

export class PopulationApiService {

	baseUrl = 'http://api.population.io:80/1.0';
	dateFormat = 'YYYY-MM-DD';

	/**
	 * Total population by country and date
	 * @param country
	 * @return {Promise<DatePopulation>}
	 */
	getPopulationByCountryToday(country) {
		const today = moment().format(this.dateFormat);
		const url = `${this.baseUrl}/population/${country}/${today}`;

		return request
			.get({url: url, json: true})
			.then(data => {
				return {
					population: data.total_population.population,
					date: today,
					country: country
				};
			});
	}

	/**
	 * Fetch aged population per year and country, transform to {Array<YearPopulation>}
	 * @param country
	 * @return {Promise<Array<YearPopulation>>}
	 */
	getPopulationByCountryForLastTwentyYears(country) {
		const today = moment();
		const years = _.range(0, 20, 2)
			.reverse()
			.map(offset => {
				return today
					.clone()
					.subtract(offset, 'year')
					.year();
			});
		const requests = years.map(year => {
			const url =  `${this.baseUrl}/population/${year}/${country}`;

			return request.get({url: url, json: true});
		});

		return Promise
			.all(requests)
			.then(data => {
				return data
					.map(yearPopulation => this.getTotalFromAged(yearPopulation));
			});

	}

	/**
	 * Transform aged population in total per year
	 * @param yearData - aged population data
	 * @returns {Array<YearPopulation>}
	 */
	getTotalFromAged(yearData) {
		const totalPopulation = yearData.reduce((total, agePopulation) => {
			return total + agePopulation.total;
		}, 0);
		const totalMalePopulation = yearData.reduce((total, agePopulation) => {
			return total + agePopulation.males;
		}, 0);
		const totalFemalePopulation = yearData.reduce((total, agePopulation) => {
			return total + agePopulation.females;
		}, 0);

		return {
			year: yearData.shift().year,
			total: totalPopulation,
			males: totalMalePopulation,
			females: totalFemalePopulation
		};
	}
}
