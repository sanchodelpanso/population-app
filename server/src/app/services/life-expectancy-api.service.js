import request from 'request-promise';
import moment from 'moment';
import * as _ from 'lodash';

/**
 * Describes avarage life expectancy by date of birth and country.
 * @typedef {Object} LifeExpectancy
 * @property {string} [dob]
 * @property {string} [country]
 * @property {number} [expectancy]
 */

export class LifeExpectancyApiService {

	baseUrl = 'http://api.population.io:80/1.0';

	/**
	 * Average life expectancy by country and date of birth
	 * @param {string} country
	 * @return {Promise<LifeExpectancy>}
	 */
	getLifeExpectancyByDateAndCountry(country, dob) {
		const requests = ['male', 'female']
			.map(sex => {
				const url = `${this.baseUrl}/life-expectancy/total/${sex}/${country}/${dob}/`;

				return request.get({url: url, json: true});
			});

		return Promise
			.all(requests)
			.then(data => {
				const total = data.reduce((acc, sexData) => {
					return acc + sexData.total_life_expectancy;
				}, 0) / data.length;

				return {
					country,
					dob,
					expectancy: total
				};
			});
	}
}
