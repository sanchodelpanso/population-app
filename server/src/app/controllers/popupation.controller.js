import {RequestController} from '../../lib/decorators/request-controller';
import {ApplicationController} from '../../lib/core/controller';
import {RequestMapping} from '../../lib/decorators/request-mapping';
import {RequestMethod} from '../../lib/core/request-method';
import {PopulationApiService} from '../services/population-api.service';

@RequestController({
	prefix: '/population',
	middleware: function(req, res, next) {
		next();
	}
})
export class PopulationController extends ApplicationController {

	service = new PopulationApiService();

	@RequestMapping({
		method: RequestMethod.GET,
		path: '/today/:country'
	})
	getTodayPopulation(req, res) {
		const {country} = req.params;

		this.service
			.getPopulationByCountryToday(country)
			.then(data => res.send(data))
			.catch(err => {
				/*
					TODO: Error handler
				 */
				res.status(400);
				res.send(err);
			});
	}

	@RequestMapping({
		method: RequestMethod.GET,
		path: '/last-twenty-years/:country'
	})
	queryForLastTwentyYears(req, res) {
		const {country} = req.params;

		this.service
			.getPopulationByCountryForLastTwentyYears(country)
			.then(data => res.send(data))
			.catch(err => {
				/*
					TODO: Error handler
				 */
				res.status(400);
				res.send(err);
			});
	}
}
