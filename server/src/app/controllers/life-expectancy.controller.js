import {RequestController} from '../../lib/decorators/request-controller';
import {ApplicationController} from '../../lib/core/controller';
import {RequestMethod} from '../../lib/core/request-method';
import {RequestMapping} from '../../lib/decorators/request-mapping';
import {LifeExpectancyApiService} from '../services/life-expectancy-api.service';

@RequestController({
	prefix: '/life-expectancy',
})
export class LifeExpectancyController extends ApplicationController {

	service = new LifeExpectancyApiService();

	@RequestMapping({
		method: RequestMethod.GET,
		path: '/:country/:dateOfBirth'
	})
	getLifeExpectancyByDateAndCountry(req, res) {
		const {country, dateOfBirth} = req.params;

		this.service
			.getLifeExpectancyByDateAndCountry(country, dateOfBirth)
			.then(data => res.send(data))
			.catch(err => {
				/*
					TODO: Error handler
				 */
				res.status(400);
				res.send(err);
			});
	}
}
