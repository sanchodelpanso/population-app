import {Application, AppConfigLogger} from './lib/app';
import * as controllers from './app/controllers';

Application.bootstrap({
	port: process.env.PORT || 8080,
	logger: AppConfigLogger.dev,
	controllers: controllers
});
