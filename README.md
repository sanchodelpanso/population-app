#Population App using Node.js, Express, React and http://api.population.io

##Development
Run both server and client application separate:

[Client React App](./client)

[Server Express App](./server)

##Run with Docker
Make sure you have installed docker and docker-compose

``
    docker-compose up
``

Open [http://localhost:4200/](http://localhost:4200/)