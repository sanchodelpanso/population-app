import React, {Component} from 'react';

import {Tab} from '../tabs/tab/tab.component';
import {Tabs} from '../tabs/tabs.component';
import {CountryStats} from '../coutry-stats/country-stats.component';

export class AppContainer extends Component {

	countries = [
		'United States',
		'Poland',
		'Ukraine'
	];

	renderTabs() {
		return this.countries.map((country, i) => {
			return (
				<Tab title={country} key={i}>
					<CountryStats country={country}/>
				</Tab>
			);
		});
	}

	render() {
		return (
			<div className='app-container pt-4'>
				<h1 className='mb-4 text-green'>COUNTRIES POPULATION DATA</h1>
				<Tabs>
					{this.renderTabs()}
				</Tabs>
			</div>
		);
	}
}
