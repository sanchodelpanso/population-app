import React, {Component} from 'react';
import PropTypes from 'prop-types';

export class DatePickerButton extends Component {

	static propTypes = {
		onClick: PropTypes.func
	};

	render () {
		return (
			<i style={{cursor: 'pointer'}} className="fa fa-calendar" onClick={this.props.onClick}/>
		);
	}
}