import React, {Component} from 'react';
import moment from 'moment';
import DatePicker from 'react-datepicker';

import {PopulationService} from '../../services/population.service';
import {PopulationChart} from './population-chart.component/population-chart.component';
import {LifeExpectancyService} from '../../services/life-expectancy.service';
import 'react-datepicker/dist/react-datepicker.css';
import {DatePickerButton} from './datepicker-button/datepicker-button.component';
import PropTypes from 'prop-types';

export class CountryStats extends Component {

	static propTypes = {
		country: PropTypes.string
	};

	populationService = new PopulationService();
	lifeExpectancyService = new LifeExpectancyService();

	state = {
		dob: moment('1952-01-01'),
		todayPopulation: 0,
		lastTwentyYearsPopulation: [],
		averageLifeExpectancy: 0
	};

	componentWillMount() {
		const {country} = this.props;

		this.populationService
			.getPopulationByCountryForLastTwentyYears(country)
			.then(data => {
				this.setState({
					...this.state,
					lastTwentyYearsPopulation: data,
				});
			});

		this.populationService
			.getTodayPopulationByCountry(country)
			.then(data => {
				this.setState({
					...this.state,
					todayPopulation: data.population,
				});
			});

		this.updateLifeExpectancy();
	}

	/**
	 * Date picker change cb
	 * @param {Moment} date
	 */
	handleChange(date) {
		this.setState({
			...this.state,
			dob: date
		}, () => this.updateLifeExpectancy());
	}

	/**
	 * Fetch life expectancy data from server, update state
	 */
	updateLifeExpectancy() {
		const {country} = this.props;
		const {dob} = this.state;

		this.lifeExpectancyService
			.getLifeExpectancyByDateAndCountry(country, dob)
			.then(data => {
				this.setState({
					...this.state,
					averageLifeExpectancy: data.expectancy
				});
			});
	}

	render() {
		const {lastTwentyYearsPopulation, todayPopulation, dob, averageLifeExpectancy} = this.state;
		const [yearFrom] = lastTwentyYearsPopulation.map(data => data.year);
		const [yearTo] = lastTwentyYearsPopulation.map(data => data.year).reverse();
		const dobString = dob.format('LL');

		return (
			<div className="country-stats">
				<h3>POPULATION IN LAST 20 YEARS</h3>
				<h5 className="mb-4 text-grey-lighten-1">{yearFrom}/{yearTo}</h5>

				<div className="d-flex justify-content-center">
					<PopulationChart population={lastTwentyYearsPopulation}/>
				</div>

				<div className="mt-4 row align-items-lg-stretch">
					<div className="col-sm-6 d-flex flex-column justify-content-between">
						<h3 className="mb-0">TOTAL POPULATION TODAY</h3>
						<div>
							<h1 className="text-lg font-weight-bold">
								{todayPopulation.toLocaleString()}
							</h1>
							<h2 className="font-weight-bold">people</h2>
						</div>
					</div>

					<div className="col-sm-6 d-flex flex-column justify-content-between">
						<h3 className="mb-0">
							AVERAGE LIFE EXPECTANCY FOR PERSON BORN ON
							<div className='font-weight-bold d-flex'>
								<span className="mr-2">{dobString}</span>
								<DatePicker selected={dob}
									customInput={<DatePickerButton/>}
									showYearDropdown
									scrollableYearDropdown
									yearDropdownItemNumber={50}
									onChange={date => this.handleChange(date)}
								/>
							</div>
						</h3>
						<div>
							<h1 className="text-lg font-weight-bold">
								{averageLifeExpectancy.toFixed(2)}
							</h1>
							<h2 className="font-weight-bold">years</h2>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
