import React, {Component} from 'react';
import {Area, AreaChart, XAxis, YAxis, CartesianGrid} from 'recharts';
import PropTypes from 'prop-types';

export class PopulationChart extends Component {

	static propTypes = {
		population: PropTypes.array
	};

	fill = '#F44256';
	stroke = '';
	height = 400;
	width = 1000;

	render() {
		const {population} = this.props;

		return (
			<AreaChart width={this.width}
					   height={this.height}
					   data={population}
			>
				<XAxis dataKey="year"/>
				<YAxis tickFormatter={tick => (tick / 1e6).toFixed(0) + 'M'}
					   width={50}
					   axisLine={false}
					   tickLine={false}
					   domain={[dataMin => (Math.floor(dataMin / 5e6) * 5e6), dataMax => (Math.ceil(dataMax / 5e6) * 5e6)]}
				/>
				<CartesianGrid height={1}/>
				<Area dataKey='total' fill={this.fill} stroke={this.stroke}/>
			</AreaChart>
		);
	}
}
