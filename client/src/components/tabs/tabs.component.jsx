import React, {Component, Children} from 'react';

import './tabs.component.scss';
import PropTypes from 'prop-types';

export class Tabs extends Component {

	static propTypes = {
		children: PropTypes.oneOfType([
			PropTypes.array,
			PropTypes.object,
			PropTypes.string
		]),
		title: PropTypes.string
	};

	state = {
		activeTabIndex: 0
	};

	handleTabClick(tabIndex) {
		this.setState({
			activeTabIndex: tabIndex
		});
	}

	renderTabsNavigation() {
		const {children} = this.props;
		const {activeTabIndex} = this.state;

		return Children.map(children, (child, index) => {
			const active = activeTabIndex === index ? 'active' : '';
			const {title} = child.props;

			return (
				<div className={active}
					onClick={() => this.handleTabClick(index)}
				>
					<h4>{title}</h4>
				</div>
			);
		});
	}

	renderTabsContent() {
		const {children} = this.props;
		const {activeTabIndex} = this.state;

		return Children.map(children, (child, index) => {
			const active = activeTabIndex === index;

			return React.cloneElement(child, {
				active: active
			});
		});
	}

	render() {
		return (
			<div className="tabs">
				<div className="tabs-nav">
					{this.renderTabsNavigation()}
				</div>
				<div className="tab-content pt-3">
					{this.renderTabsContent()}
				</div>
			</div>
		);
	}
}
