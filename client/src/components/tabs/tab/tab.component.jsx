import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './tab.component.scss';

export class Tab extends Component {

	static propTypes = {
		children: PropTypes.oneOfType([
			PropTypes.array,
			PropTypes.object,
			PropTypes.string
		]),
		active: PropTypes.bool
	};

	render() {
		const {children, active} = this.props;

		return (
			<div className={`tab-pane ${active ? 'active' : ''}`}>
				{children}
			</div>
		);
	}
}
