import {trimStart, startsWith} from 'lodash';
import 'whatwg-fetch';

import {RequestMethod} from './decorators/resource-action';

export class ResourceService {

	/**
	 * Base url for resource
	 * @type {string}
	 * @private
	 */
	_base = '';

	/**
	 * Property for temporary saved action options
	 * @type {ResourceActionOptions}
	 * @private
	 */
	_actionOptions;

	/**
	 * Main base request method, wrap around Fetch API
	 * @param {ResourceActionOptions} [actionOptions]
	 * @param {Object} requestParams - params for url, like: '/:id' -> {id: 1}
	 * @param {any} [data] - request data, attached as request body
	 * @returns {Promise<any>}
	 * @protected
	 */
	_request(requestParams = {}, data = null) {
		const request = this._getRequest(this._actionOptions, requestParams, data);

		return fetch(request)
			.then(error => this._errorInterceptor(error))
			.then(response => this._responseInterceptor(response));
	}

	/**
	 * Create new Request with provided params
	 * @param actionOptions
	 * @param params
	 * @param data
	 * @returns {Request}
	 * @private
	 */
	_getRequest(actionOptions, params, data) {
		const options = {
			...actionOptions,
			base: this._base
		};

		const method = options.method || RequestMethod.GET;
		const url = this._compileUrl(options.base, options.path, params);

		return new Request(url, {
			body: method === RequestMethod.GET ? null : (data || {}),
			method
		});
	}

	/**
	 * Method to compile url with dynamic params
	 * @param base
	 * @param path
	 * @param params
	 * @returns {string}
	 */
	_compileUrl(base, path, params) {
		if (!base) {
			throw new Error('Base url did not provided');
		}

		const url = `${base}/${trimStart(path, '/')}`;

		return url
			.split('/')
			.map(part => {
				const isParam = startsWith(part, ':');
				const isOptional = startsWith(part, '?:');
				const paramName = part.replace('?:', '').replace(':', '');
				const paramValue = params[paramName] || '';

				if (isParam && !isOptional && !paramValue) {
					throw new Error(`Parameter did not provided: ${paramName}`);
				}

				return isParam || isOptional ? paramValue : part;
			})
			.join('/');
	}

	_responseInterceptor(response) {
		return response.json();
	}

	_errorInterceptor(response) {
		if (response.status >= 400 && response.status < 600) {
			throw new Error(response.statusText);
		}

		return Promise.resolve(response);
	}
}