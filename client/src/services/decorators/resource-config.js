/**
 * Resource configuration decorator options.
 * @typedef {Object} ResourceConfigOptions
 * @property {string} [base] - resource base url
 */

/**
 * Resource configuration decorator
 * @param {ResourceConfigOptions} [options]
 * @returns {Function}
 * @constructor
 */
export function ResourceConfig(options = {}) {
	/**
	 * @param {ResourceService} target - decorated class
	 */
	return function(target) {

		return class extends target {
			constructor() {
				super();

				this._base = options.base || '';
			}
		};
	};
}
