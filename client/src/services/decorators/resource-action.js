import {ResourceService} from '../resource.service';

/**
 * Request methods enum.
 * @enum {string}
 */
export const RequestMethod = {
	GET: 'GET',
	POST: 'POST',
	PUT: 'PUT',
	PATCH: 'PATCH',
	DELETE: 'DELETE',
};

/**
 * Resource action decorator options.
 * @typedef {Object} ResourceActionOptions
 * @property {string} [path] - request path
 * @property {RequestMethod} [method=RequestMethod.GET] - request method
 */

/**
 * Resource action decorator
 * @param {ResourceActionOptions} [options]
 * @returns {Function}
 * @constructor
 */
export function ResourceAction(options = {}) {
	/**
	 * @param {Function} target - decorated method class
	 * @key {string} key - method name
	 * @key {PropertyDescriptor} descriptor - property descriptor
	 */
	return function(target, key, descriptor) {
		if(!(target.constructor.prototype instanceof ResourceService)) {
			const msg = `${target.constructor.name} does not inherit class ResourceService`;
			throw new Error(msg);
		}

		if(!(descriptor.value instanceof Function)) {
			const msg = `${target.constructor.name}#${key} is not class method, @ResourceAction could be applied only for methods`;
			throw new Error(msg);
		}

		return {
			...descriptor,
			value: function() {
				this._actionOptions = options;
				const callResult = descriptor.value.call(this, ...arguments, options);
				this._actionOptions = {};

				return callResult;
			}
		};
	};
}