import config from '../config';
import {ResourceConfig} from './decorators/resource-config';
import {RequestMethod, ResourceAction} from './decorators/resource-action';
import {ResourceService} from './resource.service';

/**
 * Describes population in particular date.
 * @typedef {Object} DatePopulation
 * @property {number} [year]
 * @property {number} [population]
 * @property {string} [country]
 */

/**
 * Describes population in particular year.
 * @typedef {Object} YearPopulation
 * @property {number} [year]
 * @property {number} [total]
 * @property {number} [male]
 * @property {number} [female]
 */

@ResourceConfig({
	base: `${config.api.host}/population`
})
export class PopulationService extends ResourceService {

	/**
	 * Today population by country name
	 * @param {string} country
	 * @param country
	 * @returns {Promise<DatePopulation>}
	 */
	@ResourceAction({
		method: RequestMethod.GET,
		path: '/today/:country'
	})
	getTodayPopulationByCountry(country) {
		return this._request({
			country
		});
	}

	/**
	 * Population for last 10 years by country name
	 * @param {string} country
	 * @return {Promise<YearPopulation[]>}
	 */
	@ResourceAction({
		method: RequestMethod.GET,
		path: '/last-twenty-years/:country'
	})
	getPopulationByCountryForLastTwentyYears(country) {
		return this._request({
			country
		});
	}
}