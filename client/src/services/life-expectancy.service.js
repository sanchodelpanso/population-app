import config from '../config';
import {ResourceConfig} from './decorators/resource-config';
import {RequestMethod, ResourceAction} from './decorators/resource-action';
import {ResourceService} from './resource.service';

/**
 * Describes avarage life expectancy by date of birth and country.
 * @typedef {Object} LifeExpectancy
 * @property {string} [dob]
 * @property {string} [country]
 * @property {number} [expectancy]
 */


@ResourceConfig({
	base: `${config.api.host}/life-expectancy`
})
export class LifeExpectancyService extends ResourceService {

	/**
	 * Date format for server request
	 * @type {string}
	 * @private
	 */
	dateFormat = 'YYYY-MM-DD';

	/**
	 * Average life expectancy by country and date of birth
	 * @param {string} country
	 * @param {Moment} date
	 * @return {Promise<LifeExpectancy>}
	 */
	@ResourceAction({
		method: RequestMethod.GET,
		path: '/:country/:date'
	})
	getLifeExpectancyByDateAndCountry(country, date) {
		return this._request({
			country,
			date: date.format(this.dateFormat)
		});
	}
}