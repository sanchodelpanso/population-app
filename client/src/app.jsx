import React from 'react';

import {AppContainer} from './components/app-container/app-container.component';

const App = () => (
	<AppContainer/>
);

export default App;
