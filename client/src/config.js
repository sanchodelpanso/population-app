export default {
	api: {
		host: process.env.NODE_ENV !== 'production' ? 'http://localhost:8080' : 'http://justcreativemotion.com:8080'
	}
};